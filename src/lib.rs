use onetime::{chacha, hash, kdf};

const HASH_COST: u32 = 12;

/// Hashes data with sha512 and bcrypt then encrypts it using chacha
/// commonly used with passwords
pub fn hash(data: &[u8], pepper: &[u8]) -> String {
    let mut nonce = onetime::token::gen_bytes(24);
    let crypt = chacha::crypt(
        &kdf::bcrypt_hash(&hash::sha512(data), HASH_COST)
            .unwrap()
            .as_bytes(),
        pepper,
        &nonce,
    );

    for b in crypt {
        nonce.push(b);
    }
    base64::encode_config(nonce, base64::BCRYPT)
}

/// Verify data with hased data
/// commonly used with passwords
pub fn verify(data: &[u8], stored: &str, pepper: &[u8]) -> bool {
    let stored = base64::decode_config(stored, base64::BCRYPT).unwrap();
    let nonce = &stored[0..24];
    let hashed = &stored[24..stored.len()];

    kdf::bcrypt_verify(
        &hash::sha512(data),
        std::str::from_utf8(&chacha::crypt(hashed, pepper, nonce)).unwrap(),
    )
    .unwrap()
}

lazy_static::lazy_static! {
    pub static ref INVALID_CHARSET: regex::Regex = regex::Regex::new(r"[^a-zA-Z0-9!@#$%^&*\-=_+]").unwrap();
    pub static ref EMAIL_REGEX: regex::Regex = regex::Regex::new(r"[\S]+@[\S]+\.[\S]+").unwrap();
}

/// Check a password with zxcvbn,
/// any score under 3 returns a suggestion
pub fn check_password(password: &str, user_inputs: &[&str]) -> Option<String> {
    if INVALID_CHARSET.is_match(password) {
        return Some(String::from("Invalid characters in Password"));
    }

    let result = zxcvbn::zxcvbn(password, user_inputs);
    if result.is_err() {
        return None;
    }

    let entropy = result.unwrap();
    if entropy.score() < 3 {
        return None;
    }

    let feedback = entropy.feedback();
    if feedback.is_none() {
        return None;
    }

    let suggestions = feedback.as_ref().unwrap().suggestions();
    if suggestions.is_empty() {
        return None;
    }

    Some(suggestions[0].to_string())
}

pub fn check_email(email: &str) -> Option<String> {
    if INVALID_CHARSET.is_match(email) {
        return Some(String::from("Invalid characters in email"));
    }

    if !EMAIL_REGEX.is_match(email) {
        return Some(String::from("Invalid email"));
    }

    None
}
